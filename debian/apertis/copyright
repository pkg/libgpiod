Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2017-2023, Bartosz Golaszewski <brgl@bgdev.pl>
License: CC-BY-SA-4.0

Files: Doxyfile
 Doxyfile.in
 Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: NEWS
Copyright: 2023, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
 2017-2021, Bartosz Golaszewski <bartekgola@gmail.com>
License: CC-BY-SA-4.0

Files: TODO
Copyright: 2017-2021, Bartosz Golaszewski <bartekgola@gmail.com>
License: CC-BY-SA-4.0

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: FSFAP and/or FSFULLR

Files: autostuff/*
Copyright: 1996-2021, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: autostuff/install-sh
Copyright: 1994, X Consortium
License: X11

Files: autostuff/ltmain.sh
Copyright: 1996-2019, 2021, 2022, Free Software Foundation, Inc.
License: (Expat and/or GPL-2+) with Libtool exception

Files: bindings/*
Copyright: 2023, Phil Howard <phil@gadgetoid.com>
License: CC-BY-SA-4.0

Files: bindings/Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: bindings/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/cxx/*
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: bindings/cxx/Makefile.am
 bindings/cxx/libgpiodcxx.pc.in
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: bindings/cxx/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/cxx/examples/*
Copyright: 2022, 2023, Kent Gibson <warthog618@gmail.com>
License: GPL-2

Files: bindings/cxx/examples/Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: bindings/cxx/examples/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/cxx/gpiodcxx/Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: bindings/cxx/gpiodcxx/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/cxx/tests/*
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: bindings/cxx/tests/Makefile.am
 bindings/cxx/tests/gpiod-cxx-test-main.cpp
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: bindings/cxx/tests/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/cxx/tests/gpiosim.cpp
 bindings/cxx/tests/gpiosim.hpp
 bindings/cxx/tests/helpers.cpp
 bindings/cxx/tests/helpers.hpp
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: bindings/python/MANIFEST.in
Copyright: 2023, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: CC0-1.0

Files: bindings/python/Makefile.am
 bindings/python/setup.py
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: bindings/python/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/python/examples/*
Copyright: 2022, 2023, Kent Gibson <warthog618@gmail.com>
License: GPL-2

Files: bindings/python/examples/Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: bindings/python/examples/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/python/gpiod/*
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: bindings/python/gpiod/Makefile.in
Copyright: 1991, 1994-2021, Free Software Foundation, Inc.
License: LGPL-2.1

Files: bindings/python/gpiod/ext/Makefile.in
Copyright: 1991, 1994-2021, Free Software Foundation, Inc.
License: LGPL-2.1

Files: bindings/python/gpiod/internal.py
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: bindings/python/gpiod/version.py
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: LGPL-2.1

Files: bindings/python/tests/*
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: bindings/python/tests/Makefile.am
 bindings/python/tests/__init__.py
 bindings/python/tests/tests_line_settings.py
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: bindings/python/tests/Makefile.in
Copyright: 1991, 1994-2021, Free Software Foundation, Inc.
License: LGPL-2.1

Files: bindings/python/tests/gpiosim/Makefile.am
 bindings/python/tests/gpiosim/ext.c
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: bindings/python/tests/gpiosim/Makefile.in
Copyright: 1991, 1994-2021, Free Software Foundation, Inc.
License: LGPL-2.1

Files: bindings/python/tests/procname/*
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: bindings/python/tests/procname/Makefile.in
Copyright: 1991, 1994-2021, Free Software Foundation, Inc.
License: LGPL-2.1

Files: bindings/python/tests/procname/__init__.py
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: bindings/rust/*
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: CC0-1.0

Files: bindings/rust/Makefile.am
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: GPL-2

Files: bindings/rust/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/gpiosim-sys/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/gpiosim-sys/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/gpiosim-sys/build.rs
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/gpiosim-sys/src/*
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/gpiosim-sys/src/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/gpiosim-sys/src/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/libgpiod-sys/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/libgpiod-sys/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/libgpiod-sys/build.rs
Copyright: 2023, Erik Schilling <erik.schilling@linaro.org>
 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, 2023, Linaro Ltd.
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/libgpiod-sys/src/*
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/libgpiod-sys/src/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/libgpiod-sys/src/lib.rs
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/libgpiod-sys/wrapper.h
Copyright: 2023, Linaro Ltd.
 2023, Erik Schilling <erik.schilling@linaro.org>
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/libgpiod/*
Copyright: 2023, Linaro Ltd.
 2023, Erik Schilling <erik.schilling@linaro.org>
 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: CC0-1.0

Files: bindings/rust/libgpiod/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/libgpiod/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/libgpiod/README.md
Copyright: 2023, Linaro Ltd.
 2023, Erik Schilling <erik.schilling@linaro.org>
License: CC0-1.0

Files: bindings/rust/libgpiod/examples/*
Copyright: 2023, Kent Gibson <warthog618@gmail.com>
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/libgpiod/examples/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/libgpiod/examples/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/libgpiod/examples/buffered_event_lifetimes.rs
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/libgpiod/src/*
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/libgpiod/src/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/libgpiod/src/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/libgpiod/tests/*
Copyright: 2022, Viresh Kumar <viresh.kumar@linaro.org>
 2022, Linaro Ltd.
License: Apache-2.0 or BSD-3-clause

Files: bindings/rust/libgpiod/tests/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/libgpiod/tests/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: bindings/rust/libgpiod/tests/common/Makefile.am
Copyright: 2022, Linaro Ltd.
 2022, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: bindings/rust/libgpiod/tests/common/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: configure.ac
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: contrib/*
Copyright: 2023, Benjamin Li <benl@squareup.com>
License: GPL-2

Files: contrib/Makefile.am
Copyright: 2023, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
License: GPL-2

Files: contrib/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: debian/*
Copyright: 2017-2024 SZ Lin (林上智) <szlin@debian.org>
            2023-2024 Gavin Lai (賴建宇) <gavin09@gmail.com>
License: LGPL-2.1+

Files: debian/patches/Don-t-run-help2man-during-cross-builds-and-use-pre-genera.patch
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: debian/patches/relicense-C++-bindings-under-LGPL-2.1-or-later.patch
Copyright: 2007, Free Software Foundation, Inc. <https://fsf.org/>
License: GPL-2 and/or LGPL-2.1 and/or LGPL-3+

Files: examples/*
Copyright: 2022, 2023, Kent Gibson <warthog618@gmail.com>
License: GPL-2

Files: examples/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: include/*
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: include/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: include/gpiod.h
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: lib/*
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: lib/Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: lib/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: lib/internal.h
Copyright: 2021, Bartosz Golaszewski <bgolaszewski@baylibre.com>
License: LGPL-2.1

Files: lib/libgpiod.pc.in
Copyright: 2018, Clemens Gruber <clemens.gruber@pqgruber.com>
 2017-2021, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: lib/misc.c
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: LGPL-2.1

Files: lib/uapi/*
Copyright: 2016, Linus Walleij
License: GPL-2

Files: m4/*
Copyright: 2004, 2005, 2007-2009, 2011-2019, 2021, 2022, Free
License: FSFULLR

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2019, 2021, 2022, Free Software
License: (FSFULLR and/or GPL-2) with Libtool exception

Files: m4/ltsugar.m4
Copyright: 2004, 2005, 2007, 2008, 2011-2019, 2021, 2022, Free Software
License: FSFULLR

Files: m4/ltversion.m4
Copyright: 2004, 2011-2019, 2021, 2022, Free Software Foundation
License: FSFULLR

Files: man/*
Copyright: Copyright (co 2017-2023, Bartosz Golaszewski
License: GPL-2

Files: man/Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: man/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: man/template
Copyright: 2017-2021, Bartosz Golaszewski <bartekgola@gmail.com>
License: CC-BY-SA-4.0

Files: tests/*
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: tests/Makefile.am
 tests/gpiod-test-helpers.c
 tests/gpiod-test-helpers.h
 tests/gpiod-test.c
 tests/gpiod-test.h
 tests/tests-line-settings.c
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: tests/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: tests/gpiod-test-sim.c
 tests/gpiod-test-sim.h
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: tests/gpiosim/*
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: GPL-2

Files: tests/gpiosim/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: tests/gpiosim/gpiosim.c
 tests/gpiosim/gpiosim.h
Copyright: 2017-2022, Bartosz Golaszewski <brgl@bgdev.pl>
License: LGPL-2.1

Files: tools/*
Copyright: 2022, Kent Gibson <warthog618@gmail.com>
 2017-2021, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: tools/Makefile.am
Copyright: 2017-2022, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: tools/Makefile.in
Copyright: 1989, 1991, 1994-2021, Free Software Foundation, Inc.
License: GPL-2

Files: tools/gpio-tools-test.bash
Copyright: 2023, Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
 2022, Kent Gibson <warthog618@gmail.com>
 2017-2021, Bartosz Golaszewski <bartekgola@gmail.com>
License: GPL-2

Files: tools/gpionotify.c
Copyright: 2022, 2023, Kent Gibson <warthog618@gmail.com>
License: GPL-2
